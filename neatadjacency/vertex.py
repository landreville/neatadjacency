from collections import namedtuple

Vertex = namedtuple('Vertex', ['x', 'y'])
