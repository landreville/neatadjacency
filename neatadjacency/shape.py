import abc


class Shape(object, metaclass=abc.ABCMeta):
    pass
