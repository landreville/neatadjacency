Neat Adjacency Reference
==============================

.. autoclass:: neatadjacency.AdjacencyList

.. autoclass:: neatadjacency.AdjacencySortedList

.. autoclass:: neatadjacency.AdjacencyListener

.. autoclass:: neatadjacency.Edge

.. autoclass:: neatadjacency.Vertex
