This is an adjacency list implementation. It's written in pure Python to ensure it 
performs great on the PyPy interpreter.

See https://landreville.gitlab.io/neatadjacency/ for reference documentation.
